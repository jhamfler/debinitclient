#!/bin/bash

# nutzer festlegen
nutzer="jh"
if [[ $# -gt 0 ]]; then
  nutzer=$1
fi

apt-get update
apt-get install sudo git -y

pfad=$(su $nutzer << 'EOF'
cd
git config --global user.name "Johannes Hamfler"
git config --global user.email "git@z7k.de"
mkdir git
cd git
git clone https://gitlab.com/jhamfler/debinitclient.git
cd debinitclient
echo $(pwd)
EOF
)

echo pfad ist "$pfad"
# rechte fixen
cd "$pfad"
cd ../.. # homedir
chown -R $nutzer:$nutzer .
chmod +x "$pfad"/init.sh
if [ -z "$2" ]
then
  "$pfad"/init.sh $nutzer $2
else
  "$pfad"/init.sh $nutzer
fi
