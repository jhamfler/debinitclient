Als `root` ausführen.

Wir wollen den Nutzer `jh` provisionieren und `alles` installieren, also auch texstudio.
Zuerst
```
su
apt install wget -y
```
dann
```
wget -O - https://gitlab.com/jhamfler/debinitclient/raw/master/preinit.sh | bash -s jh alles
```
oder
```
wget -O - i.k7z.de | bash -s jh alles
```