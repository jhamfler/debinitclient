#!/bin/bash
# erste 3 parameter
if [[ $# -lt 2 ]]
then
  echo "need at least 2 params"
  exit
fi
stunden="$1"
minuten="$2"

if [[ -z "$3" ]]
then
  sekunden=0
else
  sekunden="$3" 
fi

sec=$(echo "$stunden * 60 * 60 + $minuten * 60 + $sekunden" | bc) # ergeben Anzahl an Sekunden
unixzeit=$(echo "$(date '+%s') + $sec" | bc) # jetzt + Anzahl an Sekunden
sudo rtcwake -m mem -t "$unixzeit" # wache zu dem Zeitpunkt auf
