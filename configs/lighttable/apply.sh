#!/bin/bash
workingdir=`pwd`
destdir="$HOME/progs/lighttable"
link="https://github.com/LightTable/LightTable/releases/download/0.8.1/lighttable-0.8.1-linux.tar.gz"
if [[ -f "$destdir"/light ]]
then
  echo "$destdir already contains lighttable"
else
  mkdir -p "$destdir"
  cd "$destdir"
  wget -qO- "$link" | tar xvz
  cd light*
  mv `ls -A` ..
  cd ..
  rmdir *
fi
cd "$workingdir"
cp user.keymap "$destdir"/resources/app/core/User/user.keymap
cp user.behaviors "$destdir"/resources/app/core/User/user.behaviors
cp jh.css "$destdir"/resources/app/core/User/css/jh.css


