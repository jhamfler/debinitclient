#!/bin/bash
# alles als root

# nutzer festlegen
nutzer="jh"
if [[ $# -gt 0 ]]; then
  nutzer=$1
fi

apt-get upgrade -y

# minimum
apt-get install -y openssh-server openssh-client openssh-sftp-server
apt-get install -y awesome terminator htop byobu

# awesome in gdm anzeigen
sed -i 's/NoDisplay=true/NoDisplay=false/g' /usr/share/xsessions/awesome.desktop

#  $nutzer zu sudo hinzufügen
/usr/sbin/usermod -aG sudo $nutzer
dconf write /org/gnome/settings-daemon/plugins/cursor/active false # cursor plugin fix if present
/usr/sbin/groupadd powerdev
gpasswd -a $nutzer powerdev

# zusatz
apt-get install -y icedove firefox-esr chromium #browser
apt-get install -y spacefm #besser als nautilus
apt-get install -y conky slock ethtool arandr lm-sensors #alles für conky
apt-get install -y volti network-manager-gnome #applets
apt-get install -y pidgin vlc alarm-clock-applet x2x sshfs xbacklight #tools

if [[ "$2" == "alles" ]]; then
  # kann später nachinstalliert werden, da > 3Gb
  apt-get install -y texstudio #latex frontend
  apt-get install -y texlive   #latex bibo stretch
  apt-get install -y texlive-* #latex bibo jessie

  # nicht freies zeug
  apt-get install -y flashplugin-nonfree
fi

# small config
if grep -q "^X11Forwarding yes" /etc/ssh/sshd_config; then
  echo "x2x is ready"
else
  echo "X11Forwarding yes" | tee -a /etc/ssh/sshd_config 2>&1 >/dev/null
  echo "x2x is now ready"
fi

su $nutzer <<'EOF'

cd
cd git/debinitclient/configs/lighttable/
./apply.sh

cd
cd git
git clone https://gitlab.com/jhamfler/debinitsrv.git
git clone https://github.com/jhamfler/debconfsrv.git
git clone https://github.com/jhamfler/debconfclient.git
git clone https://gitlab.com/jhamfler/awesome.git
cd debinitsrv
./init.sh
cd ..
cd debconfsrv
./apply.sh
cd ..
cd debconfclient
./apply.sh
cd ..
cd awesome
./apply.sh
cd

EOF
